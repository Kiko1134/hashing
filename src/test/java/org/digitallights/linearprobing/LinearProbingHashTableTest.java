package org.digitallights.linearprobing;

import junit.framework.Assert;
import org.digitallights.hashtable.Iterator;
import org.digitallights.hashtable.linearprobing.LinearProbingHashTable;
import org.junit.jupiter.api.Test;

import static junit.framework.Assert.*;

public class LinearProbingHashTableTest {

    @Test
    public void TestInitialConstructor() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        assertEquals(16, hashTable.getInitialCapacity());
        assertEquals(0, hashTable.size());
    }

    @Test
    public void TestConstructorCapacity() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(64);
        assertEquals(64, hashTable.getInitialCapacity());
    }

    @Test
    public void TestConstructorCapacityAndLoadFactor() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(64, 0.80);
        assertEquals(64, hashTable.getInitialCapacity());
        assertEquals(0.80, hashTable.getLoad_factor());
    }


    @Test
    public void TestSizeWhenAdd() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("Hi");

        assertEquals(2, hashTable.size());
    }

    @Test
    public void TestSizeWhenDelete() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("Hi");

        hashTable.remove("Hi");

        assertEquals(1, hashTable.size());
    }

    @Test
    public void TestSizeWhenEmpty() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        assertEquals(0, hashTable.size());
    }

    @Test
    public void TestCapacityLoadFactorWhenInitialCapacity() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("Hell");
        hashTable.put("Hel");
        hashTable.put("He");
        hashTable.put("H");
        hashTable.put("Help");
        hashTable.put("Helps");
        hashTable.put("Helpin");
        hashTable.put("Helpi");
        hashTable.put("Half");
        hashTable.put("Homophobic");
        hashTable.put("Horse");
        hashTable.put("Hors");

        assertEquals(32, hashTable.getInitialCapacity());
    }

    @Test
    public void TestCapacityLoadFactorWhenInitialCapacityGivenByUser() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(8);
        hashTable.put("Hello");
        hashTable.put("Hell");
        hashTable.put("Hel");
        hashTable.put("He");
        hashTable.put("H");
        hashTable.put("Help");
        hashTable.put("Helpo");
        hashTable.put("Helpou");

        assertEquals(16, hashTable.getInitialCapacity());
    }

    @Test
    public void TestCapacityLoadFactorWhenBothGivenByUser() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(8, 0.625);
        hashTable.put("Hello");
        hashTable.put("Hell");
        hashTable.put("Hel");
        hashTable.put("He");
        hashTable.put("H");
        hashTable.put("Help");

        assertEquals(16, hashTable.getInitialCapacity());
    }

    @Test
    public void TestCapacityLoadFactorWhenBothGivenByUserWithMoreInputInfo() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(8, 0.625);
        hashTable.put("Hello");
        hashTable.put("Hell");
        hashTable.put("Hel");
        hashTable.put("He");
        hashTable.put("H");
        hashTable.put("Help");
        hashTable.put("Helpo");
        hashTable.put("Helpou");
        hashTable.put("Helpuo");
        hashTable.put("Helpup");
        hashTable.put("Helpip");
        hashTable.put("Helpipo");

        assertEquals(32, hashTable.getInitialCapacity());
    }

    @Test
    public void TestEmptyWhenNotEmpty() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(64);
        hashTable.put("Hello");
        hashTable.put("World");

        assertFalse(hashTable.empty());
    }

    @Test
    public void TestEmptyWhenEmpty() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(64);
        assertTrue(hashTable.empty());
    }

    @Test
    public void TestRemoveWhenKeyIsPresent() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>(64);
        hashTable.put("Hello");
        hashTable.put("World");

        hashTable.remove("Hello");

        assertEquals(1, hashTable.size());
    }

    @Test
    public void TestSizeAfterClear() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();

        hashTable.put("Hello");
        hashTable.put("World");

        hashTable.clear();

        hashTable.put("Hello");

        assertEquals(1, hashTable.size());
    }

    @Test
    public void TestAdd() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();

        hashTable.put("Hello");
        hashTable.put("World");
    }

    @Test
    public void TestContainsWhenElementPresent() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();

        hashTable.put("Hello");
        hashTable.put("World");

        assertTrue(hashTable.contains("Hello"));
    }

    @Test
    public void TestContainsWhenElementNotPresent() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();

        hashTable.put("Hello");
        hashTable.put("World");

        assertFalse(hashTable.contains("Hell"));
    }

    @Test
    public void TestContainsWhenHashtableEmpty() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        assertFalse(hashTable.contains("Hell"));
    }

    @Test
    public void TestSwapWithSameSizeHashTable() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("World");

        LinearProbingHashTable<String> hashTable2 = new LinearProbingHashTable<>();
        hashTable2.put("Name");
        hashTable2.put("Kris");

        System.out.println(hashTable.toString());
        System.out.println(hashTable2.toString());

        hashTable.swap(hashTable2);

        System.out.println(hashTable.toString());
        System.out.println(hashTable2.toString());
    }

    @Test
    public void TestSwapWithHashTablesWithDifferentSize() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("World");
        hashTable.put("FMI");
        hashTable.put("Sofia");

        LinearProbingHashTable<String> hashTable2 = new LinearProbingHashTable<>();
        hashTable2.put("Name");
        hashTable2.put("Kris");

        System.out.println(hashTable.toString());
        System.out.println(hashTable2.toString());

        hashTable.swap(hashTable2);

        System.out.println(hashTable.toString());
        System.out.println(hashTable2.toString());

        assertTrue(hashTable.contains("Kris"));
        assertTrue(hashTable2.contains("World"));
    }

    @Test
    public void TestIteratorNextAndHasNext() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("World");
        hashTable.put("FMI");
        hashTable.put("Sofia");

        Iterator<String> iterator = hashTable.iterator();

        Object[] arr = new Object[16];
        int count = 0;
        while (iterator.hasNext()) {
            arr[count] = iterator.next();
            count++;
        }

        assertEquals(4, count);
    }

    @Test
    public void TestIteratorRemove() {
        LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
        hashTable.put("Hello");
        hashTable.put("World");
        hashTable.put("FMI");
        hashTable.put("Sofia");

        Iterator<String> iterator = hashTable.iterator();

        System.out.println(hashTable.toString());

        iterator.next();
        iterator.next();
        iterator.next();
        iterator.remove();

        System.out.println(hashTable.toString());

        assertEquals(3, hashTable.size());
    }

    @Test()
    public void TestPutWithRepeatingKey() {
        try{
            LinearProbingHashTable<String> hashTable = new LinearProbingHashTable<>();
            hashTable.put("Hello");
            hashTable.put("Hello");
        }catch (IllegalArgumentException ex){
            Assert.assertEquals("This key is already in the hashtable",ex.getMessage());
        }



    }


}
