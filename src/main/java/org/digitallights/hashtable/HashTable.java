package org.digitallights.hashtable;

import java.util.NoSuchElementException;
import java.util.function.Function;

public interface HashTable<T> {
    void put(T key);

    void remove(T key) throws NoSuchElementException;

    boolean empty();

    void clear();

    int size();

    void hashFunction(Function<T, Integer> hashFunction);

    boolean contains(T key);

    void swap(HashTable<T> hashTable);

    Iterator<T> iterator();
}
