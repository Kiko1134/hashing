package org.digitallights.hashtable;

public interface Iterator<T> {
    T next();

    boolean hasNext();

    void remove();
}
