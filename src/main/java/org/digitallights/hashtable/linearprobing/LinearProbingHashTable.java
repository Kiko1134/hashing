package org.digitallights.hashtable.linearprobing;

import org.digitallights.hashtable.HashTable;
import org.digitallights.hashtable.Iterator;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Function;

/**
 * @param <T> Using generics makes this class compatible for multiple types
 */

public class LinearProbingHashTable<T> implements HashTable<T> {

    int size = 0;
    int initialCapacity = 16;
    double load_factor = 0.75;
    Function<T, Integer> hashFunction = (key) -> {
        Objects.requireNonNull(key);
        return key.hashCode();
    };

    /**
     * This function changes the hash function of the hashtable
     * @param hashFunction the hash function we want to use
     */

    @Override
    public void hashFunction(Function<T, Integer> hashFunction) {
        this.hashFunction = hashFunction;
        Object[] temp = new Object[initialCapacity];
        System.arraycopy(linear_array, 0, temp, 0, size);
        LinearProbingHashTable.this.clear();
        for(Object o: temp){
            if(o != null) LinearProbingHashTable.this.put((T)o);
        }
    }

    private Object[] linear_array;

    public double getLoadFactor() {
        return load_factor;
    }
    /**
     * This constructor gets the initial capacity and load_factor of the hashtable
     * @param initialCapacity is the given initial capacity by the user
     * @param load_factor is the given load factor by the user
     */
    public LinearProbingHashTable(int initialCapacity, double load_factor) {
        linear_array = new Object[initialCapacity];
        this.load_factor = load_factor;
        this.initialCapacity = initialCapacity;
    }
    /**
     * This constructor gives the user to choose the initial capacity
     * @param initialCapacity the capacity given by the user
     */
    public LinearProbingHashTable(int initialCapacity) {
        linear_array = new Object[initialCapacity];
        this.initialCapacity = initialCapacity;
    }

    /**
     * This constructor creates our hashtable with capacity 16 and load_factor 0.75
     */
    public LinearProbingHashTable() {
        linear_array = new Object[initialCapacity];
    }

    /**
     * This function adds element in hashtable
     * @param key is the key we want to add
     */
    @Override
    public void put(T key) {
        if(LinearProbingHashTable.this.contains(key))
            throw new IllegalArgumentException("This key is already in the hashtable");

        if ((float) size / initialCapacity >= load_factor) {
            //TODO 1. copy the array, 2. rehash, 3. replace linear_array with new array
            linear_array = Arrays.copyOf(linear_array, linear_array.length * 2);
            initialCapacity *= 2;
        }

        int hash = hashFunction.apply(key);
        int hash_index = Math.abs(hash) % initialCapacity;

        while (linear_array[hash_index] != null) {
            int done = 0;
            hash_index++;
            if (hash_index == initialCapacity) {
                for (int i = 0; i < initialCapacity; ++i) {
                    if (linear_array[i] == null) {
                        hash_index = i;
                        done = 1; //TODO can use modulo operation
                        break;
                    }
                }
            }

            if (done == 1) break;
        }

        linear_array[hash_index] = key;
        size++;
        System.out.println(hash_index);
    }

    /**
     * This removes element from the hashtable by index
     * @param key is the key of the cell we want to remove
     */
    @Override
    public void remove(T key) throws NoSuchElementException {
        int contains = 0;
        for (int i = 0; i < initialCapacity; ++i) {
            if (linear_array[i] == key) {
                linear_array[i] = null;
                size--;
                contains = 1;
            }
        }

        if (contains == 0)
            throw new NoSuchElementException("The key " + key + "is not in the hashtable");
    }

    public void removeByIndex(int index) throws IndexOutOfBoundsException {

        if (index < 0 || index > initialCapacity)
            throw new IndexOutOfBoundsException("Index is out of Bounds");

        linear_array[index] = null;
        size--;
    }

    /**
     * This function return if the hashtable contains the given key
     * @param key is the element we search if it is contained in the hashtable
     * @return true or false
     */
    @Override
    public boolean contains(T key) {
        boolean contains = false;
        for (Object o : linear_array) {
            if (o == key) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    /**
     * This function swap the values of two hashtables
     * @param hashTable is the hashtable we want to swap its values
     */
    @Override
    public void swap(HashTable<T> hashTable) {
        Object[] t;
        int t_size, t_capacity;

        t = linear_array;
        t_size = size;
        t_capacity = initialCapacity;

        linear_array = ((LinearProbingHashTable<T>)hashTable).linear_array;
        size = ((LinearProbingHashTable<T>)hashTable).size;
        initialCapacity = ((LinearProbingHashTable<T>)hashTable).initialCapacity;

        ((LinearProbingHashTable<T>)hashTable).linear_array = t;
        ((LinearProbingHashTable<T>)hashTable).size = t_size;
        ((LinearProbingHashTable<T>)hashTable).initialCapacity = t_capacity;
    }

    /**
     * This is an iterator for the hashmap
     */
    private class Iter implements Iterator<T> {

        private int pos = 0;
        private int last = 0;

        @Override
        public T next() {
            T x = (T) linear_array[pos];
            //TODO skip null positions
            last = pos;
            pos++;
            return x;
        }

        @Override
        public boolean hasNext() {
            return pos != size;
        }

        @Override
        public void remove() {
            LinearProbingHashTable.this.removeByIndex(last);
            pos--;
        }
    }

    /**
     * This function is used to initialize an iterator
     */
    @Override
    public Iterator<T> iterator() {
        return new Iter();
    }

    /**
     * This function returns the size of the hashtable
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * This function return the initial capacity of the hashtable
     */
    public int getInitialCapacity() {
        return initialCapacity;
    }

    /**
     * This function tells us if the hashtable is empty or not
     * @return true or false
     */
    @Override
    public boolean empty() {
        return size == 0;
    }

    /**
     * This function deletes all the elements from the hashtable
     */
    @Override
    public void clear() {
        size = 0;
        linear_array = new Object[initialCapacity];
    }

    /**
     * This function return the hashtable in a user-friendly way
     */
    @Override
    public String toString() {
        return "LinearProbingHashTable{" +
                "linear_array=" + Arrays.toString(linear_array) +
                '}';
    }
}
